import React from 'react';
import './App.css';
import Header from './components/Header';
import SERVICES from './components/Services';
import CONTACT from './components/Contact_Us';
import ABOUT_US from './components/About_Us';
import PAST from './components/Past_Properties';


function App() {
  return (
      <React.Fragment>
        <Header />
        <SERVICES />
        <ABOUT_US />
        <PAST/>
        <CONTACT/>
      </React.Fragment>
  );
}

export default App;
